import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Record} from "../../Interfaces/Record/record";

@Injectable({
  providedIn: 'root'
})
export class RecordService {

  private url = "http://localhost:3000/companies/"

  constructor(private http: HttpClient) { }

  getRecords(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  editRecord(record : Record){
    return this.http.put<any>(this.url + record.id, record)
  }

  createRecord(record : Record){
    return this.http.post<any>(this.url, record)
  }

  deleteRecord(id: string): Observable<any> {
    return this.http.delete<any>(this.url+id);
  }
}
