import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {SongService} from "../SongService/song.service";
import {MessageService} from "primeng/api";
import {Record} from "../../Interfaces/Record/record";
import {Artist} from "../../Interfaces/Artist/artist";

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  private url = "http://localhost:3000/artists/"

  constructor(private http: HttpClient, private songService: SongService,  private messageService: MessageService) { }

  getArtists(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  editArtist(artist : any){
    return this.http.put<any>(this.url + artist.id, artist)
  }

  createArtist( artist : any){
    return this.http.post<any>(this.url, artist)
  }

  deleteArtist(id: string): Observable<any> {
    return this.http.delete<any>(this.url+id);;
  }
}
