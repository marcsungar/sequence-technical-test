import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Song} from "../../Interfaces/Song/song";

@Injectable({
  providedIn: 'root'
})
export class SongService {

  private url = "http://localhost:3000/songs/"

  constructor(private http: HttpClient) { }

  getSongs(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  editSong(song : Song){
    return this.http.put<any>(this.url + song.id, song)
  }

  createSong( song : Song){
    return this.http.post<any>(this.url, song)
  }

  deleteSong(id: string): Observable<any> {
    return this.http.delete<any>(this.url+id);
  }

  deleteSongsByArtist(id : number): Observable<any> {
    return this.http.delete<any>(this.url+"?artist="+id);
  }
}
