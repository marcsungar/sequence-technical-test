export interface Artist {
  id : string;
  name : string;
  bornCity : string;
  birthdate : Date;
  img : string;
  rating : number;
  songs : [];
}
