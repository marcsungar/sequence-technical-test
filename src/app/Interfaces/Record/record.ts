export interface Record {
  id : string;
  name : string;
  country : string;
  createYear : number,
  employees : number,
  rating : number,
  songs : [];
}
