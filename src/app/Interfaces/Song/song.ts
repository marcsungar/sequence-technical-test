export interface Song {
  id : string;
  title : string;
  poster : string;
  genre : [ ];
  year : number;
  duration : number;
  rating : number;
  artist : number;
}
