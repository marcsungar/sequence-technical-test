import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import { HomeComponent } from './Components/home/home.component';
import {AppRoutingModule} from "./app-routing.module";
import { NavbarComponent } from './Components/navbar/navbar/navbar.component';
import {MenubarModule} from "primeng/menubar";
import {ButtonModule} from "primeng/button";
import {TagModule} from "primeng/tag";
import {CarouselModule} from "primeng/carousel";
import {RatingModule} from "primeng/rating";
import {AvatarModule} from "primeng/avatar";
import {DividerModule} from "primeng/divider";
import {FieldsetModule} from "primeng/fieldset";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";
import { CompaniesComponent } from './Components/companies/companies.component';
import {PaginatorModule} from "primeng/paginator";
import { ArtistsComponent } from './Components/artists/artists.component';
import {DataViewModule} from "primeng/dataview";
import { SongsComponent } from './Components/songs/songs.component';
import {ToastModule} from "primeng/toast";
import {ConfirmationService, MessageService} from "primeng/api";
import {SkeletonModule} from "primeng/skeleton";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {DialogModule} from "primeng/dialog";
import {InputTextModule} from "primeng/inputtext";
import {InputNumberModule} from "primeng/inputnumber";
import {FileUploadModule} from "primeng/fileupload";
import {CalendarModule} from "primeng/calendar";


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    CompaniesComponent,
    ArtistsComponent,
    SongsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    //Prime-Ng
    MenubarModule,
    ButtonModule,
    TagModule,
    CarouselModule,
    RatingModule,
    AvatarModule,
    DividerModule,
    FieldsetModule,
    FormsModule,
    PaginatorModule,
    DataViewModule,
    ToastModule,
    SkeletonModule,
    ConfirmDialogModule,
    DialogModule,
    InputTextModule,
    InputNumberModule,
    FileUploadModule,
    CalendarModule
  ],
  providers: [MessageService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
