import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./Components/home/home.component";
import {CompaniesComponent} from "./Components/companies/companies.component";
import {ArtistsComponent} from "./Components/artists/artists.component";
import {SongsComponent} from "./Components/songs/songs.component";


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'companies', component: CompaniesComponent},
  { path: 'artists', component: ArtistsComponent},
  { path: 'songs', component: SongsComponent}


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
