import {Component, OnInit} from '@angular/core';
import {Song} from "../../Interfaces/Song/song";
import {SongService} from "../../Services/SongService/song.service";
import {Artist} from "../../Interfaces/Artist/artist";
import {ArtistService} from "../../Services/ArtistService/artist.service";
import {ConfirmationService, MessageService} from "primeng/api";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {
  songs: any[] = [];
  artists: Artist [] = [];
  songsLoaded = false;
  showForm: boolean = false;
  editForm: boolean = false;
  songToEdit: Song = {id:"0",artist:0,title:"", duration:0,genre:[],poster:"",rating:0, year: 0};

//
  constructor(private songService: SongService, private artistService: ArtistService,  private messageService: MessageService, private translateService: TranslateService, private confirmationService: ConfirmationService) {}


  ngOnInit() {
    this.getData();
  }

  async getData() {
    await this.artistService.getArtists().subscribe((artist) => {
      this.artists = artist;
      this.songsLoaded = true;
    },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      });

    await this.songService.getSongs().subscribe((song) => {
      for (let aux of song) {
        for (let artist of this.artists) {
          if (artist.id == aux.artist) {
            aux.artist_name = artist.name
          }
        }
      }
      this.songs = song;
    },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      });
  }


  editSong(song: Song) {
    this.showForm = true;
    this.editForm = true;
    this.songToEdit = {id:song.id,artist:song.artist,title:song.title, duration:song.duration,genre:song.genre,poster:song.poster,rating:song.rating, year: song.year};
  }

  editSongReq(){
    this.songService.editSong(this.songToEdit).subscribe(
      (response) => {
        this.translateService.get('Forms.Song_edited').subscribe((translation: string) => {
          this.messageService.add({severity: 'success', summary: translation, detail: this.songToEdit.id.toString()});
          this.getData();
          this.showForm = false;
        });
        },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      }
    );
  }

  addSong() {
    this.showForm = true;
    this.editForm = false;
    let lastId= Number(this.songs.pop().id);
    lastId+=1;
    this.songToEdit = {id:lastId.toString(),artist:0,title:"", duration:0,genre:[],poster:"",rating:0, year: 0};
  }

  addSongReq(){
    this.songService.createSong(this.songToEdit).subscribe(
      (response) => {
        this.translateService.get('Forms.Song_created').subscribe((translation: string) => {
          this.messageService.add({severity: 'success', summary: translation, detail: this.songToEdit.id.toString()});
          this.getData();
          this.showForm = false;
        });
        },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      }
    );
  }

  deleteSong(songSelect: Song) {
    this.songService.deleteSong(songSelect.id).subscribe(
    () => {
      this.songs = this.songs.filter(song => song.id !== songSelect.id);
      this.translateService.get('Forms.Delete_success').subscribe((translation: string) => {
        this.messageService.add({ severity: 'success', summary: translation, detail: songSelect.title});
      });
    },
    (error) => {
      this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
    }
    );
  }

  deleteSongyDialog(event: Event, song: Song) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-trash',
      acceptButtonStyleClass:"p-button-danger p-button-text",
      rejectButtonStyleClass:"p-button-text p-button-text",
      acceptIcon:"none",
      rejectIcon:"none",

      accept: () => {
        this.deleteSong(song);
      },
      reject: () => {
        this.translateService.get('Forms.Operation_canceled').subscribe((translation: string) => {
          this.messageService.add({ severity: 'error', summary: translation, detail: song.title});
        });
      }
    });
  }
}
