import {Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  items: MenuItem[] | undefined;

  private labels: string [] = [];
  private icons: string [] = ['pi-home', 'pi-play', 'pi-user', 'pi-building'];
  private routerlinkmenu: string [] = ['/home', '/songs', '/artists', '/companies'];

   ngOnInit() {
    this.addItemsTranslated();
  }

  constructor(private translate: TranslateService) {
  }

  async addItemsTranslated(){
    let navbarItems: any[] = [];
    for (let i = 0; i < this.routerlinkmenu.length; i++){
     navbarItems.push(await this.searchTraduction(i));
    }
    this.items =  navbarItems;
  }

  async searchTraduction(i: number) {
    const label: string = await this.translate.get('Navbar.' + this.routerlinkmenu[i]).toPromise();
    let item = {
      label: label,
      icon: 'pi pi-fw ' + this.icons[i],
      routerLink: this.routerlinkmenu[i]
    };
    return item;
  }
}

