import {Component, OnInit} from '@angular/core';
import {Artist} from "../../Interfaces/Artist/artist";
import {ArtistService} from "../../Services/ArtistService/artist.service";
import {ConfirmationService, MessageService} from "primeng/api";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.css']
})
export class ArtistsComponent implements OnInit {
  artists: any[] = [];
  artistSelected: any = null;
  artistsLoaded: boolean  = false;
  showForm: boolean = false;
  editForm: boolean = false;
  artistToEdit: any  = { id: "0", name : "", bornCity : "", birthdate : new Date(), img : "", rating : 0, songs : []};

  constructor(private artistService: ArtistService, private messageService: MessageService, private translateService: TranslateService, private confirmationService: ConfirmationService) {
  }


  ngOnInit() {
    this.getData();
  }

  async getData() {
    await this.artistService.getArtists().subscribe((artist) => {
        this.artists = artist;
        this.artistsLoaded = true;
      },
      (error) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Request error ' + error.status,
          detail: error.statusText
        });
      });
  }

  editArtist(artist: Artist) {
    this.showForm = true;
    this.editForm = true;
    this.artistToEdit  = { id: artist.id, name : artist.name, bornCity : artist.bornCity, birthdate : artist.birthdate, img : artist.img, rating : artist.rating, songs : artist.songs};

  }
  editArtistReq(){
    let day = this.artistToEdit.birthdate.getDay()
    let month = this.artistToEdit.birthdate.getMonth();
    let year = this.artistToEdit.birthdate.getFullYear()
    this.artistToEdit.birthdate = day+"/"+month+"/"+year;
    this.artistService.editArtist(this.artistToEdit).subscribe(
      (response) => {
        this.translateService.get('Forms.Artist_edited').subscribe((translation: string) => {
          this.messageService.add({severity: 'success', summary: translation, detail: this.artistToEdit.id.toString()});
          this.getData();
          this.showForm = false;
        });
      },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      }
    );
  }


  addArtist() {
    this.showForm = true;
    this.editForm = false;
    let lastId= Number(this.artists.pop().id);
    lastId+=1;
    this.artistToEdit = { id: lastId.toString(), name : "", bornCity : "", birthdate : new Date(), img : "", rating : 0, songs : []};
  }

  addArtistReq(){
    let day = this.artistToEdit.birthdate.getDay()
    let month = this.artistToEdit.birthdate.getMonth();
    let year = this.artistToEdit.birthdate.getFullYear()
    this.artistToEdit.birthdate = day+"/"+month+"/"+year;
    this.artistService.createArtist(this.artistToEdit).subscribe(
      (response) => {
        this.translateService.get('Forms.Artist_created').subscribe((translation: string) => {
          this.messageService.add({severity: 'success', summary: translation, detail: this.artistToEdit.id.toString()});
          this.getData();
          this.showForm = false;
        });
      },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      }
    );
  }

  deleteArtist(artistSelect: Artist) {
   this.artistService.deleteArtist(artistSelect.id).subscribe(
      () => {
        this.artists = this.artists.filter(artist => artist.id !== artistSelect.id);
        this.artistSelected = null;
        this.translateService.get('Forms.Delete_success').subscribe((translation: string) => {
          this.messageService.add({severity: 'success', summary: translation, detail: artistSelect.name});
        });
      },
      (error) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Request error ' + error.status,
          detail: error.statusText
        });
      }
    );
  }


  viewArtist(id: string) {
    for (let aux of this.artists) {
      if (aux.id == id) {
        this.artistSelected = aux
      }
    }
  }

  deleteArtistDialog(event: Event, artist: Artist  ) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-trash',
      acceptButtonStyleClass: "p-button-danger p-button-text",
      rejectButtonStyleClass: "p-button-text p-button-text",
      acceptIcon: "none",
      rejectIcon: "none",

      accept: () => {
        this.deleteArtist(artist);
      },
      reject: () => {
        this.translateService.get('Forms.Operation_canceled').subscribe((translation: string) => {
          this.messageService.add({severity: 'error', summary: translation, detail: artist.name});
        });
      }
    });
  }
}
