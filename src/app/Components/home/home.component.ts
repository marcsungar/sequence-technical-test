import {Component, OnInit} from '@angular/core';
import {ArtistService} from "../../Services/ArtistService/artist.service";
import {RecordService} from "../../Services/RecordService/record.service";
import {SongService} from "../../Services/SongService/song.service";
import {Artist} from "../../Interfaces/Artist/artist";
import {Song} from "../../Interfaces/Song/song";
import {Record} from "../../Interfaces/Record/record";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  responsiveOptions: any[] | undefined;

  artists: Artist[] = [];
  songs: Song[] = [];
  records: Record[] = [];

  artistsLoaded = false;
  songsLoaded = false;
  recordsLoaded = false;


  constructor(private artistService: ArtistService, private recordService: RecordService, private songService: SongService, private messageService: MessageService) {
  }

  ngOnInit() {

    this.getData();

    this.responsiveOptions = [
      {
        breakpoint: '1199px',
        numVisible: 1,
        numScroll: 1
      },
      {
        breakpoint: '991px',
        numVisible: 2,
        numScroll: 1
      },
      {
        breakpoint: '767px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

  async getData() {
    await this.artistService.getArtists().subscribe((artists) => {
        this.artists = artists;
        this.artistsLoaded = true;
      },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      });

    await this.recordService.getRecords().subscribe((records) => {
      this.records = records;
      this.recordsLoaded = true;
    },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      });


    await this.songService.getSongs().subscribe((songs) => {
      this.songs = songs;
      this.songsLoaded = true;
    },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      });
  }

  getArtistById(id: string) {
    let artistFinded
    for (let artist of this.artists) {
      if (artist.id == id) {
        artistFinded = artist;
      }
    }
    return artistFinded;
  }
}
