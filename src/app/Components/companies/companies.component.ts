import {Component, OnInit} from '@angular/core';
import {Record} from "../../Interfaces/Record/record";
import {RecordService} from "../../Services/RecordService/record.service";
import {PaginatorState} from "primeng/paginator";
import {ConfirmationService, MessageService} from "primeng/api";
import {TranslateService} from "@ngx-translate/core";
import {Artist} from "../../Interfaces/Artist/artist";



@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit{
  records: any[] = [];
  recordsFilter: Record[] = [];
  first: number = 0;
  rows: number = 6;
  recordsLoaded = false;
  showForm: boolean = false;
  editForm: boolean = false;
  recordToEdit:  Record  = { id: "0", name : "", country : "", createYear : 0, employees : 0, rating : 0, songs : []};

  constructor(private recordService: RecordService,  private messageService: MessageService, private translateService: TranslateService, private confirmationService: ConfirmationService) {}

  ngOnInit() {
    this.getData();
  }

  async getData(){
    await this.recordService.getRecords().subscribe((records) => {
      this.records = records;
      this.recordsFilter = records.slice(this.first, this.rows);
      this.recordsLoaded = true;
    },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      });
  }

  onPageChange(event: PaginatorState) {
    if (event.first != null) {
      this.first = event.first;
    }
    if (event.rows != null) {
      this.rows = event.rows;
    }
    this.recordsFilter = this.records.slice(this.first, this.first+this.rows);

  }


  editCompany(record: Record) {
    debugger
    this.showForm = true;
    this.editForm = true;
    this.recordToEdit = {
      country: record.country,
      createYear: record.createYear,
      employees: record.employees,
      id: record.id,
      name: record.name,
      rating: record.rating,
      songs: record.songs
    }
  }
  editCompanyReq(){
    this.recordService.editRecord(this.recordToEdit).subscribe(
      (response) => {
        this.translateService.get('Forms.Record_edited').subscribe((translation: string) => {
          this.messageService.add({severity: 'success', summary: translation, detail: this.recordToEdit.id.toString()});
          this.getData();
          this.showForm = false;
        });
      },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      }
    );
  }


  addCompany() {
    this.showForm = true;
    this.editForm = false;
    let lastId= Number(this.records.pop().id);
    lastId+=1;
    this.recordToEdit = { id: lastId.toString(), name : "", country : "", createYear : 0, employees : 0, rating : 0, songs : []};
  }

  addCompanyReq(){
    this.recordService.createRecord(this.recordToEdit).subscribe(
      (response) => {
        this.translateService.get('Forms.Record_created').subscribe((translation: string) => {
          this.messageService.add({severity: 'success', summary: translation, detail: this.recordToEdit.id.toString()});
          this.getData();
          this.showForm = false;
        });
      },
      (error) => {
        this.messageService.add({ severity: 'error', summary: 'Request error ' + error.status, detail: error.statusText});
      }
    );
  }


  deleteCompany(company : Record) {
    this.recordService.deleteRecord(company.id).subscribe(
      () => {
        this.records = this.records.filter(records => records.id !== company.id);
        this.recordsFilter = this.records.slice(this.first, this.first+this.rows);
        this.translateService.get('Forms.Delete_success').subscribe((translation: string) => {
          this.messageService.add({ severity: 'success', summary: translation, detail: company.name});
        });
      },
      (error) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Request error ' + error.status,
          detail: error.statusText
        });
      }
    );
  }


  deleteCompanyDialog(event: Event, record: Record) {
      this.confirmationService.confirm({
        target: event.target as EventTarget,
        message: 'Do you want to delete this record?',
        header: 'Delete Confirmation',
        icon: 'pi pi-trash',
        acceptButtonStyleClass:"p-button-danger p-button-text",
        rejectButtonStyleClass:"p-button-text p-button-text",
        acceptIcon:"none",
        rejectIcon:"none",

        accept: () => {
            this.deleteCompany(record);
        },
        reject: () => {
          this.translateService.get('Forms.Operation_canceled').subscribe((translation: string) => {
            this.messageService.add({ severity: 'error', summary: translation, detail: record.name});
          });
        }
      });
  }
}
